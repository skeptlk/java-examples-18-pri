package com.example.ch02.tsk05;

public class MutablePoint {

    private double x, y;

    public double getX() {return x;}

    public double getY() {return y;}

    public MutablePoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public MutablePoint translate(double deltaX, double deltaY) {
        this.x += deltaX;
        this.y += deltaY;
        return this;
    }

    public MutablePoint scale(double factor) {
        this.x *= factor;
        this.y *= factor;
        return this;
    }

    @Override
    public String toString() {
        return "MutablePoint{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
