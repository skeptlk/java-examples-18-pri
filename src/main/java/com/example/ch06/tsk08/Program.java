package com.example.ch06.tsk08;


import com.example.ch05.tsk11.Pair;

import java.util.ArrayList;

/**
 * In a utility class Arrays, supply a method
 * public static <E> Pair<E> firstLast(ArrayList<___> a)
 * that returns a pair consisting of the first and last element of a. Supply an
 * appropriate type argument.
 */

public class Program {

    public static <E extends Comparable<E>> Pair<E> firstLast(ArrayList<E> a)
        throws Exception{
        if (a.size() <= 1)
            throw new Exception("Too small array");

        return new Pair<>(a.get(0), a.get(a.size() - 1));
    }

    public static void main(String[] args) throws Exception {

        ArrayList<Integer> arrayList = new ArrayList<>() {{
            add(1337); add(123) ;add(1); add(2); add(3); add(9);
        }};

        System.out.println(firstLast(arrayList));
    }
}
