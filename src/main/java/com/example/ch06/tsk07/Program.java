package com.example.ch06.tsk07;

import com.example.ch05.tsk11.Pair;

import java.util.logging.Logger;

/**
 * Implement a class Pair<E> that stores a pair of elements of type E.
 * Provide accessors to get the first and second element.
 */

/**
 * Modify the class of the preceding exercise
 * by adding methods max and min, getting the larger or smaller
 * of the two elements. Supply an appropriate type bound for E.
 */

public class Program {
    private static final Logger logger = Logger.getLogger(Program.class.getName());

    public static void main(String[] args) {
        Pair<String> pair1 = new Pair<>("Jacket", "Trousers");
        Pair<Double> pair2 = new Pair<>(420.0, 1312.0);

        logger.info("Pair 1: " + pair1);
        logger.info(pair2.toString());
        logger.info("Max in pair 1: " + pair1.max());
        logger.info("Min in pair 1: " + pair2.min());
    }
}
