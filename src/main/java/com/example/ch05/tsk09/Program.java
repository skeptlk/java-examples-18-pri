package com.example.ch05.tsk09;


import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

/**
 * Design a helper method so that one can use
 * a ReentrantLock in a try-with-resources statement.
 * Call lock and return an AutoCloseable whose close
 * method calls unlock and throws no exceptions.
 */

public class Program {

    private static final Logger logger = Logger.getLogger(Program.class.getName());

    public static AutoCloseable autoCloseReentrantLock(ReentrantLock lock) {
        return lock::unlock;
    }

    public static void main(String[] args) {
        ReentrantLock rl = new ReentrantLock();

        try (AutoCloseable ac = autoCloseReentrantLock(rl)) {

            rl.lock();
            logger.info("Locked! " + rl.isLocked());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Is locked now? " + rl.isLocked());
        }
    }
}
