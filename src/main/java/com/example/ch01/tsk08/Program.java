package src.main.java.com.example.ch01.tsk08;

import java.util.Scanner;

public class Program {
    public static void printAllSubstrings(String str) {
        for (int i = 0; i < str.length(); i++) {
            for (int j = i + 1; j <= str.length(); j++) {
                if (str.substring(i, j).trim().length() > 0)
                    System.out.println(str.substring(i, j));
            }
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        printAllSubstrings(scan.nextLine());
    }
}
