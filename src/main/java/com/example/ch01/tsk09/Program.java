package src.main.java.com.example.ch01.tsk09;

public class Program {
    public static void main(String[] args) {
        String a = "123";
        String b = "123";
        System.out.println(b);

        if (a == b) {
            System.out.println("Same object");
        } else {
            System.out.println("Not the same object");
        }
    }
}
