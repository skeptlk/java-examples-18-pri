package com.example.ch07.tsk08;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Write a program that reads all words in a file and prints out
 * on which line(s) each of them occurred. Use a map from strings to sets.
 */

public class Program {
    private final static String FILE_NAME = System.getProperty("user.home") + "/Downloads/dict.txt";

    private static final Logger logger = Logger.getLogger(Program.class.getName());

    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(new File(FILE_NAME));
            TreeMap<String, HashSet<Integer>> map = getOccurrences(sc);
            print(map);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static TreeMap<String, HashSet<Integer>> getOccurrences(Scanner sc) {
        TreeMap<String, HashSet<Integer>> map = new TreeMap<>();
        int lineNumber = 0;

        while (sc.hasNextLine()) {
            Scanner s2 = new Scanner(sc.nextLine());
            while (s2.hasNext()) {
                lineNumber++;
                String word = s2.next().toLowerCase(Locale.ROOT);

                HashSet<Integer> occurrences = map.getOrDefault(word, new HashSet<>());
                occurrences.add(lineNumber);
                map.put(word, occurrences);
            }
        }

        return map;
    }

    public static void print (TreeMap<String, HashSet<Integer>> map) {
        for(Map.Entry<String, HashSet<Integer>> entry : map.entrySet()) {
            String word = entry.getKey();
            HashSet<Integer> occurrences = entry.getValue();

            logger.info("Word \"" + word + "\" occurs on lines :");
            for (Integer lineN : occurrences) {
                logger.info(lineN + " ");
            }
            logger.info("===");
        }
    }
}