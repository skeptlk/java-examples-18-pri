package com.example.ch03.tsk08;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProgramTest {

    boolean checkSorted(ArrayList<String> list, Comparator<String> cmp) {
        boolean isSorted = false;
        String prev = null;

        for (String s: list) {
            if (prev != null &&  cmp.compare(s, prev) > 0)
                return false;
            prev = s;
        }

        return true;
    }

    @Test
    public void testSort() {
        ArrayList<String> list = new ArrayList<>() {{
            add("Lorem");
            add("ipsum");
            add("dolor");
            add("sit");
            add("amet");
            add("!");
        }};

        int size = list.size();

        Comparator<String> cmp = new Program.MyStringComparator();

        Program.luckySort(list, cmp);

        assertTrue(checkSorted(list, cmp));
        assertEquals(size, list.size());
    }
}