package com.example.ch03.tsk07;

public class Program {

    public static void main(String[] args) {
        DigitSequence seq = new DigitSequence();

        for (DigitSequence it = seq; it.hasNext(); ) {
            Integer i = it.next();

            System.out.println(i);

        }

        DigitSequenceIterable seqIterable = new DigitSequenceIterable();

        for (Integer i: seqIterable) {
            System.out.println(i);
        }

    }


}
