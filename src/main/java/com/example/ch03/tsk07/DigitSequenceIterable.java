package com.example.ch03.tsk07;

import java.util.Iterator;

public class DigitSequenceIterable implements Iterable<Integer> {
    @Override
    public Iterator<Integer> iterator() {
        System.out.println("Iterator called!");
        return new DigitSequence();
    }
}
