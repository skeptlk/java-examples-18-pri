package com.example.ch08.tsk10;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 *  Given a finite stream of strings, find the average string length.
 */

public class Program {
    public static Double averageStringLength(Stream<String> stream) {
        return stream
                .mapToDouble(String::length)
                .average()
                .orElse(Double.NaN);
    }

    public static void main(String[] args) {
        String[] strings = {"Hello", "sen"};
        System.out.println(averageStringLength(Arrays.stream(strings)));
    }
}
