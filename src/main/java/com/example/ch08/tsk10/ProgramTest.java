package com.example.ch08.tsk10;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.*;

public class ProgramTest {
    @Test
    public void averageStringLength () {
        String[] strings = {"Hello", "sen"};

        Double average = Program.averageStringLength(Arrays.stream(strings));

        assertEquals(4.0, (double) average, 0.0001);
    }

    @Test
    public void averageStringLengthNaN () {
        Double average = Program.averageStringLength(Arrays.stream(new String[0]));

        assertTrue(average.isNaN());
    }
}
