package com.example.ch08.tsk09;

import java.util.Scanner;
import org.junit.Test;
import static org.junit.Assert.*;

public class ProgramTest {
    @Test
    public void getWordsWithNVowels () {
        String data = """
                aaaaa ooooo abeecofu
                abc ... 123 abc
                abc anacondaaz
        """;

        String[] words = Program.getWordsWithNVowels(new Scanner(data), 5);

        assertEquals(4, words.length);

        assertArrayEquals(new String[] {"aaaaa", "ooooo", "abeecofu", "anacondaaz"}, words);
    }
}
