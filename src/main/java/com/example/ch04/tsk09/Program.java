package com.example.ch04.tsk09;

import com.example.ch02.tsk05.Point;

import java.lang.reflect.Field;
import java.util.Objects;
import java.util.logging.Logger;

class Circle extends Point {
    private double radius = 0.0;
    public Circle(double rad, double x, double y) {
        super(x, y);
        this.radius = rad;
    }
}

public class Program {

    private static final Logger logger = Logger.getLogger(Program.class.getName());

    /**
     * Write a “universal” toString method that uses reflection
     * to yield a string with all instance variables of an object.
     * Extra credit if you can handle cyclic references.
     *
     * @param object
     * @return
     */
    public static String toString(Object object) {
        StringBuilder result = new StringBuilder();
        String newLine = System.getProperty("line.separator");

        Class<?> cl = object.getClass();
        while(cl != null) {
            Field[] fields = cl.getDeclaredFields();

            for (Field field : fields) {
                try {
                    field.setAccessible(true);
                    Object v = field.get(object);
                    if (!Objects.equals(object, v) && v != null) {
                        result.append(field).append(" = ").append(v);
                        result.append(newLine);
                    }
                } catch (Exception e) {}
            }

            cl = cl.getSuperclass();
        }

        return result.toString();
    }

    public static void main(String[] args) {
        logger.info(toString(new Circle(10.0, 1, 2)));
    }
}
