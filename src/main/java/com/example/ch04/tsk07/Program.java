package com.example.ch04.tsk07;

/**
 * Define an enumeration type for the eight combinations of primary colors
 * BLACK, RED, BLUE, GREEN, CYAN, MAGENTA, YELLOW, WHITE
 * with methods getRed, getGreen, and getBlue.
 */

public class Program {
    public static void main(String[] args) {
        System.out.println(MyColor.BLUE.getBlue());
        System.out.println(MyColor.MAGENTA.getGreen());
    }
}
