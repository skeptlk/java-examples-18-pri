package com.example.ch04.tsk10;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class MethodPrinter {
    public static String all(String className) {
        StringBuilder result = new StringBuilder();

        try {
            Class<?> cl = Class.forName(className);
            while (cl != null) {
                for (Method m : cl.getDeclaredMethods()) {
                    result.append(Modifier.toString(m.getModifiers()))
                          .append(" ")
                          .append(m.getReturnType().getCanonicalName())
                          .append(" ")
                          .append(m.getName())
                          .append(Arrays.toString(m.getParameters()))
                          .append("\n");
                }
                cl = cl.getSuperclass();
            }
        } catch (Exception e) {}

        return result.toString();
    }
}
